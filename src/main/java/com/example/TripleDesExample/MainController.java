package com.example.TripleDesExample;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/TripleDes")
public class MainController {

    final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);

    public MainController() throws Exception {
    }

    @PostMapping(value = "/test", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> Test(@RequestBody(required = false) String body, HttpServletRequest request) {
        try {
            final String key = tripleDES.decrypt(request.getHeader("key"));
            System.out.println("hasil " +new TripleDES(key).encrypt(body));
            final ResponseEntity<String> response = new ResponseEntity<>("berhasil", HttpStatus.BAD_GATEWAY);
            return response;
        }catch (Exception e){
            return null;
        }
    }

}
