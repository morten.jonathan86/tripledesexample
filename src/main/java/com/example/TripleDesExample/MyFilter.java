package com.example.TripleDesExample;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import static javax.xml.transform.OutputKeys.ENCODING;

@Component
public class MyFilter extends OncePerRequestFilter {


    final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);

    public MyFilter() throws Exception {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            final String keyTrans = new TripleDES(Constants.TDES_KEY_APP).decrypt(httpServletRequest.getHeader("key"));
            System.out.println("asd the key "+keyTrans);
            //check if it contain body req
            if (httpServletRequest.getContentLength() > -1) {
                 httpServletRequest = new RequestWrapper(httpServletRequest,keyTrans);
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (Exception e) {
            System.out.println("asd Oops, something went wrong \n" + e.getMessage());
            e.printStackTrace();
        }


    }
}
