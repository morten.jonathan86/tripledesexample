package com.example.TripleDesExample;



import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
public class SessionSave implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper((HttpServletRequest) request);
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper((HttpServletResponse) response);

        try {
            chain.doFilter(requestWrapper, responseWrapper);
        } finally {

            try{
                String responseBody = new String(responseWrapper.getContentAsByteArray());
                final String keyTrans = new TripleDES(Constants.TDES_KEY_APP).decrypt(((HttpServletRequest) request).getHeader("key"));
                final int httpStatus = ((HttpServletResponse) response).getStatus();
                String content = new TripleDES(keyTrans).encrypt(responseBody);
                System.out.println("asd http status "+((HttpServletResponse) response).getStatus());
                responseWrapper.reset();
                responseWrapper.setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE);
                responseWrapper.setContentLength(content.length());
                responseWrapper.setStatus(httpStatus);
                responseWrapper.getOutputStream().write(content.getBytes());
                responseWrapper.copyBodyToResponse();
                System.out.println("asd masuk");
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

}
