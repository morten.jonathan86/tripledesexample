package com.example.TripleDesExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripleDesExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TripleDesExampleApplication.class, args);
		System.out.println("SERVICE STARTED::..");
	}

}
